require 'sinatra'
require 'redcarpet'

get '/' do
	raw = File.open("essay.md", "rb").read
	renderer = Redcarpet::Render::HTML.new
	parser = Redcarpet::Markdown.new(renderer, {})
	'<link href="https://raw.github.com/clownfart/Markdown-CSS/master/markdown.css" rel="stylesheet"></link>' + parser.render(raw)
end